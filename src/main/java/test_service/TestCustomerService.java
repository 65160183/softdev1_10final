/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author Windows10
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        
        for(Customer customer :cs.getCustomer()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0888868888"));
        Customer cus1 = new Customer("kob", "0869999999");
        cs.addNew(cus1);
        for(Customer customer :cs.getCustomer()){
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0869999999") ;
        delCus.setTel("0869999990");
        cs.update(delCus);
        System.out.println("After Updated");
                
        for(Customer customer :cs.getCustomer()){
            System.out.println(customer);
        }
        cs.delete(delCus);
                for(Customer customer :cs.getCustomer()){
            System.out.println(customer);
        }
    }
}
